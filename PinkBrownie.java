package ai;
import robocode.*;
import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.DeathEvent;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;
import robocode.util.Utils;


public class PinkBrownie extends AdvancedRobot
{

	static final int patDep = 28;
    static String eLog = "00000000000000000000088887654321088";
    static double eEner;
    static int moveNum = 0;
    static int bestMove = 1;
    static double bestMove1 = 0.0D;
    static int startStop;
    static double bPow;
    static double mvAmt;
    static double moveDir = 1.0D;
	

	//turn radar left until you find enemy, if enemy found stop and only put radar "close" to enemy
    public void run() {
	
		double wallDist = Math.min(Math.min(this.getX(), this.getY()), Math.min(this.getBattleFieldWidth() - this.getX(), this.getBattleFieldHeight() - this.getY()));
        
        if (wallDist < 42.0D) {
            this.setBack(100.0D * moveDir);
        }
		
        this.setAdjustGunForRobotTurn(true);
        this.setAdjustRadarForGunTurn(true);
        this.setColors(Color.yellow.darker(), Color.yellow, Color.yellow.darker());

        while(true) {
            this.turnRadarLeftRadians(1.0D);
            this.scan();
        }
    }


	//if radar scannes enemy
	public void onScannedRobot(ScannedRobotEvent e) {
	
		double absB = e.getBearingRadians();
        int mLen = 28;
        double wallDist = Math.min(Math.min(this.getX(), this.getY()), Math.min(this.getBattleFieldWidth() - this.getX(), this.getBattleFieldHeight() - this.getY()));
        mvAmt = (Math.random() * 700.0D + 500.0D) / 2.0D;
        this.setMaxVelocity(8.0D * Math.random() + 5.0D);

        if (e.getDistance() > 50.0D) {
            this.setTurnRightRadians(Math.cos(e.getBearingRadians()));
        }

        if (wallDist < 42.0D) {
            this.setBack(100.0D * moveDir);
        }

        if (moveNum < 100 && eEner > (eEner = e.getEnergy())) {
            this.setAhead(Math.random() * mvAmt - mvAmt * 0.5D);
        }

        if (moveNum >= 100 && moveNum < 200) {
            this.setAhead((double)(10 * startStop) * moveDir);
            if (eEner > (eEner = e.getEnergy())) {
                if (startStop == 0) {
                    startStop = 1;
                } else {
                    startStop = 0;
                }
            }
        }

        if (moveNum >= 200 && bestMove != 0) {
            bestMove = 0;
            if (bestMove1 < 0.0D) {
                moveNum = 150;
            } else {
                moveNum = 0;
            }
        }

        absB = e.getBearingRadians();
        eLog = String.valueOf((char)((int)Math.round(e.getVelocity() * Math.sin(e.getHeadingRadians() - (absB += this.getHeadingRadians()))))).concat(eLog);

        int i;
        int indX;
        while((indX = eLog.indexOf(eLog.substring(0, mLen--), i = (int)(e.getDistance() / (20.0D - 3.0D * bPow)))) < 0) {
            ;
        }

        do {
            absB += Math.asin((double)((byte)eLog.charAt(indX--)) / e.getDistance());
            --i;
        } while(i > 0);

        this.setTurnGunRightRadians(Utils.normalRelativeAngle(absB - this.getGunHeadingRadians()));
        
		//smart fire
		smartFire(e.getDistance());

        double radarTurn = this.getHeadingRadians() + e.getBearingRadians() - this.getRadarHeadingRadians();
        this.setTurnRadarRightRadians(2.0D * Utils.normalRelativeAngle(radarTurn));
	}
	
	//fire Power depens on enemy distance and robo Energy
	public void smartFire(double robotDistance) {
		if (robotDistance > 200 || getEnergy() < 15) {
			fire(1);
		} else if (robotDistance > 50) {
			fire(2);
		} else {
			fire(3);
		}
	}

	//escape enemy
	//turn by random value and set back
	public void onHitByBullet(HitByBulletEvent e) {
		mvAmt = (Math.random() * 700.0D + 500.0D) / 2.0D;
		back(Math.random() * mvAmt - mvAmt * 0.5D);
	}
	
	//if robo hits the wall
	public void onHitWall(HitWallEvent e) {
        moveDir *= -1.0D;
	}	
	
	//turn by random value and set back/ahead
	public void onHitRobot(HitRobotEvent e) {
		 mvAmt = (Math.random() * 700.0D + 500.0D) / 2.0D;

		// If he's in front of us, set back up a bit.
		if (e.getBearing() > -90 && e.getBearing() < 90) {
			back(Math.random() * mvAmt - mvAmt * 0.5D);
		} // else he's in back of us, so set ahead a bit.
		else {
			ahead(Math.random() * mvAmt - mvAmt * 0.5D);
		}
	}
	
	//Save between rounds
    public void onDeath(DeathEvent event) {
        moveNum += 50 * bestMove;
        if (moveNum < 100) {
            bestMove1 -= eEner;
        } else {
            bestMove1 += eEner;
        }
    }

	//Save between rounds
    public void onWin(WinEvent event) {
        if (moveNum < 100) {
            bestMove1 += this.getEnergy();
        } else {
            bestMove1 -= this.getEnergy();
        }

    }
}
